package praktikum3;

public class LCD {
    private String Status;
    private int Volume;
    private int Brightness;
    private String Cable;
    
    public String turnOff(){
        return this.Status = "Mati"; 
    }
    
    public String turnOn(){
        return this.Status = "Menyala";
    }
    
    public String Freeze(){
        return this.Status = "Freeze";
    }
    
    public int volumeUp(){
        return this.Volume = Volume++;
    }
    
    public int volumeDown(){
        return this.Volume = Volume--;
    }
    
    public int setVolume(int volume){
        return this.Volume = volume;
    }
    
    public int brightnessUp(){
        return this.Brightness = Brightness++;
    }
    
    public int brightnessDown(){
        return this.Brightness = Brightness--;
    }
    
    public int setBrightness(int brightness){
        return this.Brightness = brightness;
    }
    
    public void cableUp(){
        if(this.Cable == "HDMI"){this.Cable = "VGA";}
        else if(this.Cable == "VGA"){this.Cable = "DVI";}
        else if(this.Cable == "DVI"){this.Cable = "DVI";}
    }
    
    public void cableDown(){
        if(this.Cable == "DVI"){this.Cable = "VGA";
        } else if(this.Cable == "VGA"){this.Cable = "HDMI";}
        else if(this.Cable == "HDMI"){this.Cable = "HDMI";}
    }
    
    public void setCable(String cable){
        this.Cable = cable;
    }
    
    public void displayMessage(){
        System.out.println("-------------- LCD --------------");
        System.out.println("Status LCD saat ini    : " + Status);
        System.out.println("Volume LCD saat ini    : " + Volume);
        System.out.println("Brightness             : " + Brightness);
        System.out.println("Cable                  : " + Cable);
    }
    
    
    
    public static void main(String[] args){
        LCD lcd1 = new LCD();
        lcd1.turnOff();
        lcd1.turnOn();
        lcd1.setCable("HDMI");
        lcd1.cableUp();
        lcd1.setVolume(50);
        lcd1.Freeze();
        lcd1.setBrightness(20);
        
        lcd1.displayMessage();
        
        
        
    }
    
}
